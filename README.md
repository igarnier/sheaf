# Graph reconstruction

```
- lib/bheap.ml              modified binary heap (originally from JC Filliatre)
     /digraph.ml            directed graph implementation
     /intf.ml               module types
     /mux.ml                multiplex several streams and sort them into one
     /reconstruction.ml     construct a stream of graphs from a stream of neighbourhoods
     /statistics.ml         basic functions on graphs (connected components, shortest paths, etc)
```

```
- bin/bounded_queue.ml      basic bounded queue, used in GUI
     /display.ml            GUI main loop
     /helpers.ml            misc.
     /main.ml               implement commands on top of `bin/reconstruct.ml`
     /picking.ml            GUI handling clicking on stuff
     /reconstruct.ml        instantiate `lib/reconstruction.ml` on encoding for Tezos P2P logs
     /render_graph.ml       GUI: graph rendering
     /render_gui.ml         GUI: render canvas
     /springmodel.ml        GUI: graph dynamics
```

## Architecture

- The library handles
  - merging streams (`mux.ml`)
  - constructing a stream of graphs from a stream of neighbourhoods (`reconstruct.ml`)
  - computing basic stuff on graphs (`statistics.ml`)

- the binary instantiate the library on streams of logs from Tezos experiments
  (currently the tool consumes streams of neighbourhood dumps) and performs various
  stuff on these streams:
  - GUI (with picking, displaying peer id and metadata)
  - various commands
