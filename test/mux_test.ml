open Sheaf

module Row = struct
  type t = float

  let compare = Float.compare
end

module Stream = struct
  type t = float Seq.t

  type elt = float

  type error = unit

  let pp_error _ () = ()

  let next s = match Seq.uncons s with None -> Ok None | Some x -> Ok (Some x)
end

module Muxer = Mux.Make (Row) (Stream)
open QCheck2

let float_array =
  Gen.map
    (fun a ->
      Array.sort Row.compare a ;
      a)
    (Gen.array Gen.float)

let float_array_array = Gen.small_array float_array

let fusion arr_arr =
  let res = Array.concat (Array.to_list arr_arr) in
  Array.sort Row.compare res ;
  res

let preserve_order arr_arr =
  match
    Muxer.create
      (Array.mapi (fun i x -> (string_of_int i, Array.to_seq x)) arr_arr)
  with
  | (stream, { bad_states = [] }) ->
      let arr =
        Seq.unfold
          (fun x -> match Muxer.next x with Ok x -> x | Error _ -> None)
          stream
        |> Array.of_seq
      in
      arr = fusion arr_arr
  | (_, { bad_states }) ->
      ( List.for_all @@ fun (_, _, err) ->
        match err with
        | Muxer.Stream_empty ->
            (* That's not really an error *)
            true
        | Muxer.Stream_error _ -> assert false )
        bad_states

let () = assert (preserve_order [||])

let () = assert (preserve_order [| [| 0.0; 2.0; 3.0 |]; [| 3.0; 4.0; 5.0 |] |])

let () = assert (preserve_order [| [| 1.0; 2.0; 3.0 |]; [| 1.1; 2.1; 3.1 |] |])

let () = assert (preserve_order [| [| 1.1; 2.1; 3.1 |]; [| 1.0; 2.0; 3.0 |] |])

let preserve_order =
  QCheck2.Test.make
    ~count:100
    ~name:"preserves_order"
    float_array_array
    preserve_order

let qcheck_wrap = List.map QCheck_alcotest.to_alcotest

let () =
  Alcotest.run
    "demux_test"
    [("order preservation", qcheck_wrap [preserve_order])]
