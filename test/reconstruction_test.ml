module G = Sheaf.Digraph

(* Test that the graph implementation distinguishes edge directionality *)

let distinguish_directionality () =
  let g = G.create () in
  G.add_vertex g 0 ;
  G.add_vertex g 1 ;
  G.add_edge g 0 1 ;
  G.mem_edge g 0 1 && not (G.mem_edge g 1 0)

(* Test that we can have edges going both ways *)

let supports_bidirectionality () =
  let g = G.create () in
  G.add_vertex g 0 ;
  G.add_vertex g 1 ;
  G.add_edge g 0 1 ;
  G.add_edge g 1 0 ;
  G.mem_edge g 0 1 && G.mem_edge g 1 0

let () = assert (distinguish_directionality ())

let () = assert (supports_bidirectionality ())
