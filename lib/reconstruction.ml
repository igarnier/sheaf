type diff =
  { vertex : Digraph.vertex;
    added_neighbours : Digraph.vertex list;
    removed_neighbours : Digraph.vertex list
  }

module Make (Node : Intf.Node) (S : Intf.Stream with type elt = Node.t) :
  Intf.Reconstructed_stream
    with type id = Node.Id.t
     and type meta = Node.meta
     and type diff = diff
     and type local_stream = S.t
     and type name = string = struct
  module N =
    Mux.Make
      (struct
        type t = Node.t

        let compare n1 n2 = Float.compare (Node.time n1) (Node.time n2)
      end)
      (S)

  type id = Node.Id.t

  type meta = Node.meta

  type nonrec diff = diff

  type local_stream = S.t

  type name = string

  type elt =
    { t : float;
      graph : Digraph.t;
      ids : Node.Id.t Digraph.V.Map.t Lazy.t;
      metadata : Digraph.vertex -> Node.meta option;
      diff : diff;
      self : Node.Id.t
    }

  type t = { prev : elt option; changes : N.t }

  type local_error = N.local_error

  type error = N.error = { bad_states : (string * S.t * local_error) list }

  let pp_error = N.pp_error

  let create ns =
    match N.create ns with (changes, errs) -> ({ prev = None; changes }, errs)

  let gen =
    let c = ref 0 in
    fun () ->
      let v = !c in
      incr c ;
      v

  module Node_table = Hashtbl.Make (Node.Id)
  module Vertex_table = Hashtbl.Make (Digraph.V)

  (* A table implementing an injection from nodes to integer ids (used as vertices) *)
  let node_indices = Node_table.create 11

  let meta_table = Vertex_table.create 11

  let vertex node =
    match Node_table.find_opt node_indices node with
    | None ->
        let i = gen () in
        Node_table.add node_indices node i ;
        i
    | Some i -> i

  let mem_id (elt : Digraph.vertex) (arr : Digraph.vertex array) =
    let len = Array.length arr in
    if len = 0 then false
    else
      let rec loop elt arr start stop =
        if stop < start then false
        else
          let i = start + ((stop - start + 1) / 2) in
          let c = Digraph.V.compare arr.(i) elt in
          if c = 0 then true
          else if c < 0 then loop elt arr (i + 1) stop
          else loop elt arr start (i - 1)
      in
      loop elt arr 0 (len - 1)

  let set_neighbourhood graph time vertex (neighbours : Digraph.vertex array) =
    if Digraph.mem_vertex graph vertex then (
      let _ = Hashtbl.create (Digraph.out_degree graph vertex) in
      let old_neighbours =
        Digraph.fold_succ (fun v acc -> v :: acc) graph vertex []
      in
      let removed =
        List.fold_left
          (fun removed v ->
            if not (mem_id v neighbours) then v :: removed else removed)
          []
          old_neighbours
      in
      let added =
        Array.fold_left
          (fun added v ->
            if not (Digraph.mem_edge graph vertex v) then v :: added else added)
          []
          neighbours
      in
      let diff =
        { vertex; added_neighbours = added; removed_neighbours = removed }
      in
      List.iter (fun v -> Digraph.remove_edge graph vertex v) removed ;
      List.iter
        (fun v ->
          assert (v <> vertex) ;
          Digraph.add_edge graph vertex v)
        added ;
      diff)
    else (
      Digraph.add_vertex graph vertex ;
      Array.iter (fun v -> Digraph.add_edge graph vertex v) neighbours ;
      { vertex;
        added_neighbours = Array.to_list neighbours;
        removed_neighbours = []
      })

  let metadata = Digraph.V.Table.create 800

  let next { prev; changes } =
    match N.next changes with
    | Result.Error e as err -> err
    | Result.Ok None as empty -> empty
    | Result.Ok (Some (n, changes)) ->
        let (prev_graph, prev_metadata, prev_ids) =
          match prev with
          | None ->
              ( Digraph.create (),
                (fun v -> Digraph.V.Table.find_opt metadata v),
                Lazy.from_val Digraph.V.Map.empty )
          | Some m -> (m.graph, m.metadata, m.ids)
        in
        let time = Node.time n in
        let v = vertex (Node.self n) in
        let () =
          match Node.meta n with
          | None -> Digraph.V.Table.remove metadata v
          | Some meta -> Digraph.V.Table.replace metadata v meta
        in
        let neighbours = Node.neighbours n in
        let ids =
          Lazy.from_fun @@ fun () ->
          Array.fold_left
            (fun ids id -> Digraph.V.Map.add (vertex id) id ids)
            (Lazy.force prev_ids)
            neighbours
        in
        let graph = prev_graph in
        let diff =
          set_neighbourhood prev_graph time v (Array.map vertex neighbours)
        in
        let elt =
          { t = time;
            graph;
            ids;
            metadata = prev_metadata;
            diff;
            self = Node.self n
          }
        in
        Result.ok (Some (elt, { prev = Some elt; changes }))
end
