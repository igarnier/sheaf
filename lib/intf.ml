(** Types admitting a total order and a dummy value. *)
module type OrderedType = sig
  type t

  val compare : t -> t -> int
end

(** A module type packing a total order and a hash function.
    Suitable for building maps and/or hashtables. *)
module type Std = sig
  type t

  val compare : t -> t -> int

  val equal : t -> t -> bool

  val hash : t -> int

  val pp : Format.formatter -> t -> unit
end

(** The module type of graph nodes consumed by the reconstruction module. *)
module type Node = sig
  (** The type of nodes. *)
  type t

  (** The type of metadata associated to a node. *)
  type meta

  (** The module type of node ids. *)
  module Id : Std

  (** [meta node] returns the metadata of [node], if any. *)
  val meta : t -> meta option

  (** [self node] returns the id of [node]. *)
  val self : t -> Id.t

  (** [neighbours node] returns the ids of [node]'s neighbours.
      The array is sorted in increasing [Id.t] order. *)
  val neighbours : t -> Id.t array

  (** [time node] returns the time at which this node was sampled*)
  val time : t -> float

  (** Pretty-printing. *)
  val pp : Format.formatter -> t -> unit

  val pp_meta : Format.formatter -> meta -> unit
end

(** The module type of streams. *)
module type Stream = sig
  (** The type of streams. *)
  type t

  (** The type of elements being streamed. *)
  type elt

  (** The type of errors *)
  type error

  (** Get the next element out of the stream. *)
  val next : t -> ((elt * t) option, error) Result.t

  val pp_error : Format.formatter -> error -> unit
end

(** The module type of multiplexed streams. *)
module type Mux_stream = sig
  (** The type of errors that occur when consuming local strams. *)
  type local_error

  (** The type of streams being multiplexed into a single one. *)
  type local_stream

  type error = { bad_states : (string * local_stream * local_error) list }

  include Stream with type error := error

  type name

  val create : (string * local_stream) array -> t * error
end

(** The module type of streamed reconstructed graphs *)
module type Reconstructed_stream = sig
  (** The type of node ids. *)
  type id

  (** The type of metadata. *)
  type meta

  (** The type of graph diffs, corresponding to adding/removing edges around a vertex. *)
  type diff

  (** The type of elements contained in the reconstructed stream. *)
  type elt =
    { t : float;
      graph : Digraph.t;
      ids : id Digraph.V.Map.t Lazy.t;
      metadata : Digraph.vertex -> meta option;
      diff : diff;
      self : id
    }

  include Mux_stream with type elt := elt and type name = string

  val vertex : id -> Digraph.vertex

  (** Construct a reconstructed stream from an array of local streams.
      The function returns a stream containing the multiplexed contents
      of all localstreams that could be accessed. Erroneous streams are
      also returned together with a description of the failure. *)
  val create : (name * local_stream) array -> t * error
end
