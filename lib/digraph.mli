type t

module V : sig
  type t = int

  val compare : t -> t -> int

  val equal : t -> t -> bool

  val hash : t -> int

  val pp : Format.formatter -> t -> unit

  module Table : Hashtbl.S with type key = t

  module Set : Set.S with type elt = t

  module Map : Map.S with type key = t
end

module E : sig
  type t

  type label

  val label : t -> label

  val src : t -> V.t

  val dst : t -> V.t

  val create : V.t -> label -> V.t -> t
end

type vertex = V.t

type edge = E.t

val create : ?size:int -> unit -> t

val out_degree : t -> vertex -> int

val nb_vertex : t -> int

val nb_edges : t -> int

val mem_vertex : t -> vertex -> bool

val mem_edge : t -> vertex -> vertex -> bool

val add_vertex : t -> vertex -> unit

val add_edge : t -> vertex -> vertex -> unit

val remove_edge : t -> vertex -> vertex -> unit

val iter_vertex : (vertex -> unit) -> t -> unit

val iter_edges : (vertex -> vertex -> unit) -> t -> unit

val succ : t -> vertex -> vertex list

val succ_e : t -> vertex -> edge list

val fold_vertex : (vertex -> 'a -> 'a) -> t -> 'a -> 'a

val fold_edges : (vertex -> vertex -> 'a -> 'a) -> t -> 'a -> 'a

val fold_edges_e : (edge -> 'a -> 'a) -> t -> 'a -> 'a

val iter_succ : (vertex -> unit) -> t -> vertex -> unit

val iter_succ_e : (edge -> unit) -> t -> vertex -> unit

val fold_succ : (vertex -> 'a -> 'a) -> t -> vertex -> 'a -> 'a
