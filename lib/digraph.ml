include Graph.Imperative.Digraph.Concrete (struct
  include Int

  let hash = Hashtbl.hash
end)

module V = struct
  include V

  let pp = Format.pp_print_int

  module Table = Hashtbl.Make (V)
  module Set = Set.Make (V)
  module Map = Map.Make (V)
end
