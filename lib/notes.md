# Network topology visualization

## Input

For each file:
- timestamp self-info (peer-info)*

## Preprocessed

### File structure

For each file:
- timestamp info-diff peer-info-diff*

info-diff:
- topics diff = (topic x bool) set = (topic -> bool) map

peer-info-diff:
    new peer = full info
    existing peer = peer id x topics diff
    removed peer = peer id

omit timestamps with no diffs

### Processing algorithm

- maintain current state = self x neighbourhod
  - self = id x topics x private x ...
  - neighbourhood = id -> topics x private x ...

- on new row (timestamp self-info (peer-info)*)
  - compute diff
    - self-diff = topics-diff x private-diff x ...
      - topics-diff (X, Y) = X\Y + Y\X
    - neighbourhood diff =
      - ...
  - (timestamp self-diff peer-diffs)
  - apply diff

## UX

> tool preprocess trace.txt --outfile trace.bin --id

Produces trace.bin

## Display

- N stream of timestamps to fusion

### Binary stream fusion

- t_0 ... t_{n-1} \ u_0 ... u_{n-1}
=> take minimum: 1 compare

- three streams
  - A. insert streams in priority heap
  - B. take min
  - C. if new value of min still below new min, loop B.
       else
        - insert stream back in heap
        - loop B

- should we pull from this stream or should the stream push
  to the consumer at a frequency set by the consumer?

### Display/Processing

diff (A x B) = diff A x diff B
diff (A + B) = diff A + diff B
diff 1 = 1
diff bool = 2
