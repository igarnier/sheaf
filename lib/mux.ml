open Intf

module Make (Elt : OrderedType) (Stream : Stream with type elt = Elt.t) : sig
  type local_error = Stream_error of Stream.error | Stream_empty

  include
    Mux_stream
      with type name = string
       and type local_error := local_error
       and type local_stream = Stream.t
       and type elt = Elt.t
end = struct
  type local_stream = Stream.t

  type local_error = Stream_error of Stream.error | Stream_empty

  type elt = Elt.t

  type name = string

  module In_stream = struct
    type t = Uninitialized | Stream of { head : Elt.t; stream : Stream.t }

    let compare stream1 stream2 =
      match (stream1, stream2) with
      | (Uninitialized, Uninitialized) -> 0
      | (Uninitialized, _) -> -1
      | (_, Uninitialized) -> 1
      | (Stream { head = h1; _ }, Stream { head = h2; _ }) -> Elt.compare h1 h2

    (* NB: [of_state] returns an error or an initialized stream *)
    let of_state s =
      match Stream.next s with
      | Ok None -> Error Stream_empty
      | Ok (Some (head, stream)) -> Ok (Stream { head; stream })
      | Error err -> Error (Stream_error err)

    let dummy = Uninitialized
  end

  type error = { bad_states : (string * Stream.t * local_error) list }

  let pp_error fmtr { bad_states } =
    Format.pp_print_list
      ~pp_sep:(fun fmtr () -> Format.fprintf fmtr "@ ")
      (fun fmtr (name, _, err) ->
        match err with
        | Stream_error err ->
            Format.fprintf fmtr "Stream %s: %a" name Stream.pp_error err
        | Stream_empty -> Format.fprintf fmtr "Stream %s: empty" name)
      fmtr
      bad_states

  module Heap = Bheap.Make (In_stream)

  type t = Heap.t

  let create states =
    let (ok_states, bad_states) =
      Array.fold_left
        (fun (acc, bad_states) (name, h) ->
          match In_stream.of_state h with
          | Error e -> (acc, (name, h, e) :: bad_states)
          | Ok In_stream.Uninitialized ->
              (* [of_state] can't return an unitialized stream *)
              assert false
          | Ok s -> ((name, h, s) :: acc, bad_states))
        ([], [])
        states
    in
    let states =
      Array.of_list (List.map (fun (name, _, x) -> (name, x)) ok_states)
    in
    let heap = Heap.create ~dummy:In_stream.dummy (Array.length states) in
    Array.iter (fun (_name, stream) -> Heap.add heap stream) states ;
    (heap, { bad_states })

  let next heap =
    match Heap.minimum heap with
    | exception Bheap.Empty -> Result.ok None
    | In_stream.Uninitialized -> assert false
    | In_stream.(Stream { head; stream }) ->
        let res = head in
        let () =
          match Stream.next stream with
          | Error err ->
              (* This stream has encountered an error - carry on *)
              Format.eprintf
                "Error encountered: %a - removing stream@."
                Stream.pp_error
                err ;
              Heap.remove heap
          | Ok None ->
              (* This stream has become empty *)
              Heap.remove heap
          | Ok (Some (new_head, new_stream)) -> (
              (* If [new_head] is still below the next smallest element,
                 no need to reorganize the heap (this is an optimization) *)
              match Heap.next_smallest heap with
              | None ->
                  (* No other element in the heap - no need to reorg *)
                  heap.data.(0) <-
                    In_stream.Stream { head = new_head; stream = new_stream }
              | Some In_stream.Uninitialized -> assert false
              | Some (In_stream.Stream next) ->
                  if Elt.compare new_head next.head = -1 then
                    (* New head is still below next smallest element *)
                    heap.data.(0) <-
                      In_stream.Stream { head = new_head; stream = new_stream }
                  else (
                    (* New head is not the smallest one, switch *)
                    Heap.remove heap ;
                    Heap.add
                      heap
                      (In_stream.Stream { head = new_head; stream = new_stream }))
              )
        in
        Result.Ok (Some (res, heap))
end
