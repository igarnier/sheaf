include Stats.Graph.Make (Digraph)
module SCC = Graph.Components.Make (Digraph)
module CC = Graph.Components.Undirected (Digraph)

module Path =
  Graph.Path.Dijkstra
    (Digraph)
    (struct
      type edge = Digraph.edge

      type t = unit

      let weight _ = ()

      let compare () () = 0

      let add () () = ()

      let zero = ()
    end)

let connected_components graph = CC.components_array graph

let strongly_connected_components graph = SCC.scc graph

let path_length graph v0 v1 =
  try Some (List.length (fst (Path.shortest_path graph v0 v1))) with _ -> None
