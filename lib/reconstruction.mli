(** The type of graph [diff]s, corresponding to adding/removing edges around a vertex *)
type diff =
  { vertex : Digraph.vertex;
    added_neighbours : Digraph.vertex list;
    removed_neighbours : Digraph.vertex list
  }

module Make : functor
  (Node : Intf.Node)
  (S : Intf.Stream with type elt = Node.t)
  ->
  Intf.Reconstructed_stream
    with type id = Node.Id.t
     and type meta = Node.meta
     and type diff = diff
     and type local_stream = S.t
