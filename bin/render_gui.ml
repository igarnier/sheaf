type button_pressed_events = Elapse_pressed | Relax_pressed | Nothing_pressed

type gui_outputs =
  { button_pressed_events : button_pressed_events;
    sliders : Springmodel.simulation_parameters
  }

(* 75 150 *)

let slider_radius (current : Springmodel.simulation_parameters) x y =
  Raygui.slider
    (Raylib.Rectangle.create x y 100. 10.)
    "radius"
    ""
    current.radius
    ~min:10.
    ~max:300.

let slider_stiffness (current : Springmodel.simulation_parameters) x y =
  Raygui.slider
    (Raylib.Rectangle.create x y 100. 10.)
    "stiffness"
    ""
    current.stiffness
    ~min:1.
    ~max:300.

let slider_edge_length (current : Springmodel.simulation_parameters) x y =
  Raygui.slider
    (Raylib.Rectangle.create x y 100. 10.)
    "edge length"
    ""
    current.relax_length
    ~min:1.
    ~max:300.

let slider_drag_coeff (current : Springmodel.simulation_parameters) x y =
  Raygui.slider
    (Raylib.Rectangle.create x y 100. 10.)
    "drag"
    ""
    current.drag_factor
    ~min:1.
    ~max:300.

let slider_coulomb_coeff (current : Springmodel.simulation_parameters) x y =
  Raygui.slider
    (Raylib.Rectangle.create x y 100. 10.)
    "coulomb"
    ""
    current.coulomb_factor
    ~min:1.
    ~max:1000.

let slider_simspeed (current : Springmodel.simulation_parameters) x y =
  Raygui.slider
    (Raylib.Rectangle.create x y 100. 10.)
    "simspeed"
    ""
    (current.dt *. 100.)
    ~min:0.
    ~max:1.

let sliders current x y0 =
  let slider_radius = slider_radius current x y0 in
  let slider_stiffness = slider_stiffness current x (y0 +. 20.) in
  let slider_edge_length = slider_edge_length current x (y0 +. 40.) in
  let slider_drag_coeff = slider_drag_coeff current x (y0 +. 60.) in
  let slider_coulomb_coeff = slider_coulomb_coeff current x (y0 +. 80.) in
  let slider_simspeed = slider_simspeed current x (y0 +. 100.) in
  let open Springmodel in
  { radius = slider_radius;
    stiffness = slider_stiffness;
    relax_length = slider_edge_length;
    drag_factor = slider_drag_coeff;
    coulomb_factor = slider_coulomb_coeff;
    dt = 0.01 *. slider_simspeed
  }

let draw_gui ~graph ~diff_queue ~layout ~sm_params ~pinned ~picked ~camera ~now
    ~metadata =
  let open Raylib in
  begin_drawing () ;
  clear_background Color.raywhite ;
  (* draw graph *)
  begin_mode_3d camera ;
  Render_graph.draw_scene ~graph ~diff_queue ~layout ~pinned ~picked ~camera ;
  draw_grid 100 10.0 ;
  end_mode_3d () ;
  (* draw gui *)
  draw_rectangle 10 10 320 133 (fade Color.skyblue 0.5) ;
  draw_rectangle_lines 10 10 320 133 Color.blue ;
  draw_text "Free camera default controls:" 20 20 10 Color.black ;
  draw_text "- Mouse Wheel to Zoom in-out" 40 40 10 Color.darkgray ;
  draw_text "- Mouse Wheel Pressed to Pan" 40 60 10 Color.darkgray ;
  draw_text "- Alt + Mouse Wheel Pressed to Rotate" 40 80 10 Color.darkgray ;
  draw_text
    "- Alt + Ctrl + Mouse Wheel Pressed for Smooth Zoom"
    40
    100
    10
    Color.darkgray ;
  draw_text "- Z to zoom to (0 0 0)" 40 120 10 Color.darkgray ;
  let width = get_screen_width () in
  let height = get_screen_height () in
  let button_width = 200 in
  let button_height = 125 in
  let offset = 50 in

  let () =
    match picked with
    | None -> ()
    | Some v ->
        draw_text
          (match metadata v with
          | None -> "unknown"
          | Some meta -> Format.asprintf "%a" Reconstruct.Input.pp_meta meta)
          (width - 200 - button_width)
          100
          20
          Color.darkgray
  in

  let rect =
    Rectangle.create
      (float @@ (width - offset - button_width))
      (float @@ (height - offset - button_height))
      (float button_width)
      (float button_height)
  in
  let elapse = Raygui.button rect "Step forward" in

  let rect =
    Rectangle.create
      (float offset)
      (float (height - offset - button_height))
      (float button_width)
      (float button_height)
  in
  let relax = Raygui.button rect "Relax layout" in

  draw_text
    (Format.asprintf "time: %f s" now)
    (width - (2 * offset) - button_width)
    20
    30
    Color.black ;

  let sliders = sliders sm_params 75. 150. in

  end_drawing () ;

  let button_pressed_events =
    if elapse then (
      assert (not relax) ;
      Elapse_pressed)
    else if relax then (
      assert (not elapse) ;
      Relax_pressed)
    else Nothing_pressed
  in
  { button_pressed_events; sliders }

open Sheaf

type inputs =
  { graph : Reconstruct.elt Cgraph.Var.t;
    mutable pinned : Digraph.V.Set.t Cgraph.Var.t;
    mutable picked : Digraph.vertex option Cgraph.Var.t;
    sm_params : Springmodel.simulation_parameters Cgraph.Var.t;
    sm_evolve : bool Cgraph.Var.t;
    render_refresh : unit Cgraph.Var.t;
    camera : Raylib.Camera3D.t Cgraph.t;
    mutable now : float;
    mutable ray : Raylib.Ray.t option;
    ray_trigger : unit Cgraph.Var.t
  }

type outputs =
  { layout : Springmodel.layout Cgraph.t;
    render : gui_outputs Cgraph.t;
    picked : Digraph.vertex option Cgraph.t
  }

let prepare init_graph sm_params camera =
  { graph = Cgraph.Var.create init_graph;
    pinned = Cgraph.Var.create Digraph.V.Set.empty;
    picked = Cgraph.Var.create None;
    sm_params = Cgraph.Var.create sm_params;
    sm_evolve = Cgraph.Var.create false;
    render_refresh = Cgraph.Var.create ();
    camera;
    now = 0.0;
    ray = None;
    ray_trigger = Cgraph.Var.create ()
  }

let process input =
  let graph = Cgraph.var input.graph in
  let sm_evolve = Cgraph.var input.sm_evolve in
  let render_refresh = Cgraph.var input.render_refresh in
  let ray_trigger = Cgraph.var input.ray_trigger in
  let pinned = Cgraph.var input.pinned in
  let sm_params = Cgraph.var input.sm_params in
  let picked = Cgraph.var input.picked in
  let layout =
    Springmodel.process ~pinned ~sm_params ~sm_graph:graph ~evolve:sm_evolve
  in
  let diff_queue = Bounded_queue.create 45 in
  let picked =
    let open Cgraph.Infix in
    let+ () = ray_trigger in
    let picked = Cgraph.get picked in
    let ({ graph; _ } : Reconstruct.elt) = Cgraph.get graph in
    let layout = Cgraph.get layout in
    match (picked, input.ray) with
    | (Some _, Some _) ->
        input.ray <- None ;
        None
    | (Some _, None) -> picked
    | (None, Some ray) ->
        (* TODO hardcoded constant 5.0 *)
        let res = Picking.intersect ~graph ~layout ~box_size:5.0 ~ray in
        input.ray <- None ;
        res
    | (None, None) -> None
  in
  let render =
    let open Cgraph.Infix in
    let+ { graph; diff; metadata; _ } = graph
    and+ layout
    and+ () = render_refresh
    and+ picked
    and+ pinned
    and+ camera = input.camera
    and+ sm_params in
    Bounded_queue.push diff diff_queue ;
    draw_gui
      ~graph
      ~diff_queue
      ~layout
      ~sm_params
      ~pinned
      ~picked
      ~camera
      ~now:input.now
      ~metadata
  in
  { layout; render; picked }
