(* [Simulator_input] is used to consume the output of the simulator - it is currently
   dead code. *)
module Simulator_input : sig
  include Sheaf.Intf.Node with type meta = unit and type Id.t = int

  val encoding : t Data_encoding.t
end = struct
  type t = { now : float; id : int; neighbourhood : int array }

  type meta = unit

  module Id = struct
    include Int

    let hash = Hashtbl.hash

    let pp = Format.pp_print_int
  end

  let meta _ = None

  let self { id; _ } = id

  let neighbours { neighbourhood; _ } = neighbourhood

  let time { now; _ } = now

  let pp fmtr { now; id; neighbourhood } =
    Format.fprintf
      fmtr
      "@[<v 2>{ now = %f;@,id = %d;@,neighbourhood = [%a]@ }@]"
      now
      id
      (Format.pp_print_list
         ~pp_sep:(fun fmtr () -> Format.fprintf fmtr ",")
         Format.pp_print_int)
      (Array.to_list neighbourhood)

  let pp_meta _fmtr () = ()

  let encoding : t Data_encoding.t =
    let open Data_encoding in
    conv
      (fun { now; id; neighbourhood } -> (now, id, neighbourhood))
      (fun (now, id, neighbourhood) -> { now; id; neighbourhood })
      (obj3
         (req "now" float)
         (req "id" int31)
         (req "neighbourhood" (array int31)))
end

let integer_encoding =
  Data_encoding.conv Int32.of_int Int32.to_int Data_encoding.int32

module Int_set = struct
  include Set.Make (Int)

  let encoding =
    let open Data_encoding in
    conv elements of_list (list integer_encoding)
end

module Input : sig
  type dim = Fin of Int_set.t | All

  type topic = { shard : dim; slot : dim; level : dim }

  val topic_of_string : string -> topic option

  val topic_intersects : topic -> topic -> bool

  include Sheaf.Intf.Node with type meta = string * topic and type Id.t = string

  val encoding : t Data_encoding.t
end = struct
  type dim = Fin of Int_set.t | All

  type point = { addr : string; port : int }

  type topic = { shard : dim; slot : dim; level : dim }

  type peer =
    { id : string; id_point : point; trusted : bool; topics : topic option }

  type t = { node_id : string; date : float; topic : topic; peers : peer array }

  type meta = string * topic

  module Id = struct
    include String

    let hash = Hashtbl.hash

    let pp = Format.pp_print_string
  end

  let meta { node_id; topic; _ } = Some (node_id, topic)

  let self { node_id; _ } = node_id

  let neighbours { peers; _ } = Array.map (fun { id; _ } -> id) peers

  let time { date; _ } = date

  (* Note: most of the functions dealing with topics below were copied from `lib_base/p2p_topic.ml` *)

  let pp_dim fmtr (d : dim) =
    match d with
    | Fin topics ->
        (Format.pp_print_list
           ~pp_sep:(fun fmtr () -> Format.fprintf fmtr ",")
           Format.pp_print_int)
          fmtr
          (Int_set.elements topics)
    | All -> Format.fprintf fmtr "*"

  let pp_topic fmtr { shard; slot; level } =
    Format.fprintf
      fmtr
      "@[<v 2>{ shard = %a;@,slot = %a;@,level = %a@ }@]"
      pp_dim
      shard
      pp_dim
      slot
      pp_dim
      level

  let dim_of_string s =
    match s with
    | "*" -> Some All
    | s -> (
        let elts = String.split_on_char ',' s in
        try Some (Fin (Int_set.of_list (List.map int_of_string elts)))
        with _ -> None)

  let topic_of_string str =
    let components = String.split_on_char '/' str in
    match components with
    | [shard; slot; level] ->
        Option.bind (dim_of_string shard) @@ fun shard ->
        Option.bind (dim_of_string slot) @@ fun slot ->
        Option.bind (dim_of_string level) @@ fun level ->
        Option.some { shard; slot; level }
    | _ -> None

  let dim_intersects x y =
    match (x, y) with
    | (_, All) | (All, _) -> true
    | (Fin s1, Fin s2) -> not (Int_set.disjoint s1 s2)

  let topic_intersects t1 t2 =
    dim_intersects t1.shard t2.shard
    && dim_intersects t1.slot t2.slot
    && dim_intersects t1.level t2.level

  let pp fmtr ({ date; node_id; topic; peers = _ } as x) =
    Format.fprintf
      fmtr
      "@[<v 2>{ date = %f;@,\
       node_id = %s;@,\
       topics = %a;@,\
       neighbourhood = [%a]@ }@]"
      date
      node_id
      pp_topic
      topic
      (Format.pp_print_list
         ~pp_sep:(fun fmtr () -> Format.fprintf fmtr ",")
         Format.pp_print_string)
      (Array.to_list @@ neighbours x)

  let pp_meta fmtr (id, topic) =
    Format.fprintf fmtr "@[<v>id = %s@,%a@]" id pp_topic topic

  let point : point Data_encoding.t =
    let open Data_encoding in
    conv
      (fun { addr; port } -> (addr, port))
      (fun (addr, port) -> { addr; port })
      (obj2 (req "addr" string) (req "port" int31))

  let dim_encoding : dim Data_encoding.t =
    let open Data_encoding in
    union
      ~tag_size:`Uint8
      [ case
          ~title:"all"
          (Tag 0)
          (obj1 (req "kind" (constant "all")))
          (function All -> Some () | Fin _ -> None)
          (fun () -> All);
        case
          ~title:"fin"
          (Tag 1)
          (obj1 (req "fin" Int_set.encoding))
          (function Fin f -> Some f | All -> None)
          (fun set -> Fin set) ]

  let topic =
    let open Data_encoding in
    conv
      (fun { shard; slot; level } -> (shard, slot, level))
      (fun (shard, slot, level) -> { shard; slot; level })
      (obj3
         (req "shard" dim_encoding)
         (req "slot" dim_encoding)
         (req "level" dim_encoding))

  let peers : peer Data_encoding.t =
    let open Data_encoding in
    conv
      (fun { id; id_point; trusted; topics } -> (id, id_point, trusted, topics))
      (fun (id, id_point, trusted, topics) -> { id; id_point; trusted; topics })
      (obj4
         (req "id" string)
         (req "id_point" point)
         (req "trusted" bool)
         (req "topics" (option topic)))

  let encoding : t Data_encoding.t =
    let open Data_encoding in
    conv
      (fun { date; node_id; topic; peers } -> (date, node_id, topic, peers))
      (fun (date, node_id, topic, peers) ->
        Array.sort (fun p1 p2 -> String.compare p1.id p2.id) peers ;
        { date; node_id; topic; peers })
      (obj4
         (req "date" float)
         (req "node_id" string)
         (req "topics" topic)
         (req "peers" (array peers)))
end

module Input_stream = struct
  type 'a seq = unit -> 'a node_with_errors

  and +'a node_with_errors = Nil | Cons of 'a * 'a seq | Error of string

  type t = Input.t Seq.t

  type elt = Input.t

  let next x =
    match Seq.uncons x with
    | None -> Result.Ok None
    | Some x -> Result.Ok (Some x)

  type error = unit

  let pp_error _ () = ()

  let from_channel fd =
    let buf = Buffer.create 2048 in
    let stream =
      Yojson.Basic.seq_from_channel ~buf fd |> Seq.map Helpers.of_yojson
    in
    Seq.of_dispenser (fun () ->
        let rec loop stream =
          match Seq.uncons stream with
          | None -> None
          | Some (json, rest) -> (
              match json with
              | `O [("fd-sink-item.v0", `O fields)] -> (
                  match List.assoc_opt "event" fields with
                  | Some (`O [("peers_dumping.v0", json)]) ->
                      let row =
                        try Data_encoding.Json.destruct Input.encoding json
                        with e ->
                          Format.eprintf "%a@." Data_encoding.Json.pp json ;
                          raise e
                      in
                      Some row
                  | None -> loop rest
                  | Some _ -> loop rest)
              | _ -> loop stream)
        in
        loop stream)
end

include Sheaf.Reconstruction.Make (Input) (Input_stream)
