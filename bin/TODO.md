# TODOs

## Nice-to-have
- GUI: display applicative layer message propagation
- Springmodel: drag does not obey a physically accurate law, current impl. does not ensure energy dissipation
  - would need an implicit integration scheme

## Important
- GUI: font size adjustement
- GUI: allow to increase stream consumption rate
- Springmodel: not convinced repulsive potential is useful, remove it

## Critical
- implement reader for peer dumping format
- sheaf: handle selection of subgraph by metadata
  - implemented streaming graph with metadata on nodes
- sheaf: robustness statistics on graph/topic subgraph
  - diameter of graph/topic subgraph
  - property 1: connectedness
  - property 2: robustness of graph/topic subgraph
    - minimum amount of edges to remove to disconnect: `edge-connectivity`
    - see https://en.wikipedia.org/wiki/K-edge-connected_graph
    - see https://en.wikipedia.org/wiki/Karger%27s_algorithm (compute min cut)
  - property 3: cut diversity (number of distinct topics a given subgraph is connected to)
  - property 4: min cut size over all topic subgraphs (most weakly connected topic subgraph)
    - properties 3, 4 equiv. to degree stats over the graph with nodes = topics and weighted edges = cut size
