type 'a t = { queue : 'a Queue.t; max_length : int }

let create max_length = { queue = Queue.create (); max_length }

let push elt { queue; max_length } =
  if Queue.length queue < max_length then Queue.add elt queue
  else (
    Queue.add elt queue ;
    ignore (Queue.pop queue))

let iteri f { queue; _ } = Queue.to_seq queue |> Seq.iteri f
