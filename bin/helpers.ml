let rec of_yojson : Yojson.Basic.t -> Data_encoding.Json.t =
 fun json ->
  match json with
  | `Bool b -> `Bool b
  | `Null -> `Null
  | `Assoc fields -> `O (List.map (fun (x, y) -> (x, of_yojson y)) fields)
  | `List fields -> `A (List.map of_yojson fields)
  | `Float f -> `Float f
  | `String s -> `String s
  | `Int i -> `Float (float_of_int i)

let rec to_yojson : Data_encoding.Json.t -> Yojson.Basic.t =
 fun json ->
  match json with
  | `Bool b -> `Bool b
  | `Null -> `Null
  | `O fields -> `Assoc (List.map (fun (x, y) -> (x, to_yojson y)) fields)
  | `A fields -> `List (List.map to_yojson fields)
  | `Float f -> `Float f
  | `String s -> `String s
