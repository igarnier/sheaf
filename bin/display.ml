open Sheaf

(* -------------------------------------------------------------------------- *)

type global_state =
  { display_inputs : Render_gui.inputs;
    display_outputs : Render_gui.outputs;
    mutable consume_switch : bool;
    mutable stream : Reconstruct.t;
    camera : Raylib.Camera.t Cgraph.Var.t
  }

let setup_window width height =
  let open Raylib in
  init_window width height "p2p sim" ;
  let camera =
    Camera.create
      (Vector3.create 50.0 50.0 50.0) (* position *)
      (Vector3.create 0.0 0.0 0.0) (* target *)
      (Vector3.create 0.0 1.0 0.0) (* up *)
      45.0
      (* FOV *) CameraProjection.Perspective
  in
  set_camera_mode camera CameraMode.Free ;
  set_target_fps 60 ;
  camera

let prepare_global width height sm_params stream =
  match Reconstruct.next stream with
  | Result.Ok (Some (elt, stream)) ->
      let camera = Cgraph.Var.create (setup_window width height) in
      let inputs = Render_gui.prepare elt sm_params (Cgraph.var camera) in
      { display_inputs = inputs;
        display_outputs = Render_gui.process inputs;
        consume_switch = false;
        stream;
        camera
      }
  | Result.Ok None | Result.Error _ ->
      Format.eprintf "prepare_global: invalid stream, can't initialize gui@." ;
      exit 1

let maybe_reset_camera { camera; _ } =
  let open Raylib in
  if is_key_down Key.Z then
    (* Reset cam if Z is pressed *)
    Cgraph.Var.set
      camera
      Camera.(
        create
          (Vector3.create 50.0 50.0 50.0)
          (Vector3.create 0.0 0.0 0.0) (* target *)
          (up (Cgraph.Var.peek camera))
          45.0
          (* FOV *) CameraProjection.Perspective)
  else () ;
  update_camera (addr (Cgraph.Var.peek camera))

let elapse_toggle button_pressed =
  let open Raylib in
  is_key_pressed Key.Space || button_pressed = Render_gui.Elapse_pressed

let relax_toggle button_pressed =
  let open Raylib in
  is_key_pressed Key.R || button_pressed = Render_gui.Relax_pressed

let pinned_toggle () = Raylib.(is_key_pressed Key.P)

let step global_state =
  let open Raylib in
  if window_should_close () then (
    close_window () ;
    exit 0) ;

  maybe_reset_camera global_state ;

  let () =
    if global_state.consume_switch then (
      match Reconstruct.next global_state.stream with
      | Error _ | Ok None -> ()
      | Ok (Some (update, rest)) ->
          Cgraph.Var.set global_state.display_inputs.graph update ;
          global_state.display_inputs.now <- update.t ;
          global_state.stream <- rest)
    else ()
  in

  let () =
    (* This weird form checks if a variable is true, and if so
       forces recomputation of the dependent computation by re-setting it. *)
    if Cgraph.Var.peek global_state.display_inputs.sm_evolve then
      Cgraph.Var.set global_state.display_inputs.sm_evolve true
    else ()
  in

  let left_clicked = is_mouse_button_released MouseButton.Left in

  let { Render_gui.button_pressed_events; sliders } =
    Cgraph.Var.set global_state.display_inputs.render_refresh () ;
    (* This performs a call to the rendering functions *)
    Cgraph.get global_state.display_outputs.render
  in

  let () =
    if left_clicked && button_pressed_events = Nothing_pressed then (
      Cgraph.Var.set global_state.display_inputs.ray_trigger () ;
      global_state.display_inputs.ray <-
        Some
          (get_mouse_ray
             (get_mouse_position ())
             (Cgraph.Var.peek global_state.camera)))
    else ()
  in

  let picked = Cgraph.get global_state.display_outputs.picked in
  let current_picked = Cgraph.Var.peek global_state.display_inputs.picked in
  if Option.equal Int.equal picked current_picked then ()
  else Cgraph.Var.set global_state.display_inputs.picked picked ;

  if elapse_toggle button_pressed_events then
    global_state.consume_switch <- not global_state.consume_switch
  else () ;

  if relax_toggle button_pressed_events then
    let v = Cgraph.Var.peek global_state.display_inputs.sm_evolve in
    Cgraph.Var.set global_state.display_inputs.sm_evolve (not v)
  else () ;

  Cgraph.Var.set global_state.display_inputs.sm_params sliders ;

  if pinned_toggle () then
    let i = global_state.display_inputs in
    let pinned = Cgraph.Var.peek i.pinned in
    match picked with
    | None -> ()
    | Some v ->
        if Digraph.V.Set.mem v pinned then
          Cgraph.Var.set i.pinned (Digraph.V.Set.remove v pinned)
        else Cgraph.Var.set i.pinned (Digraph.V.Set.add v pinned)
  else ()

let rec loop state =
  step state ;
  loop state
