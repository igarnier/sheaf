let box_around_vertex (box_size : float) (pos : Gg.P3.t) =
  let open Raylib in
  let px = Gg.P3.x pos in
  let py = Gg.P3.y pos in
  let pz = Gg.P3.z pos in
  let hsize = 0.5 *. box_size in
  BoundingBox.create
    Vector3.(create (px -. hsize) (py -. hsize) (pz -. hsize))
    Vector3.(create (px +. hsize) (py +. hsize) (pz +. hsize))

let intersect ~(graph : Sheaf.Digraph.t) ~(layout : Springmodel.layout)
    ~(box_size : float) ~(ray : Raylib.Ray.t) =
  let exception Early_exit of Sheaf.Digraph.vertex in
  try
    Sheaf.Digraph.iter_vertex
      (fun v ->
        let pos = layout v in
        let box = box_around_vertex box_size pos in
        let coll = Raylib.get_ray_collision_box ray box in
        if Raylib.RayCollision.hit coll then raise (Early_exit v))
      graph ;
    None
  with Early_exit v -> Some v
