[@@@ocaml.warning "+27"]

open Sheaf

(* Merges many stream into one, the merging function is specified
   by the first-class module argument *)
let reconstruct :
    type t s elt.
    (module Intf.Mux_stream
       with type local_stream = s
        and type t = t
        and type elt = elt) ->
    (in_channel -> s) ->
    string list ->
    (t -> 'a) ->
    'a =
 fun (module R) from_channel files k ->
  let ics =
    List.map
      (fun file ->
        let stream = In_channel.open_text file |> from_channel in
        (file, stream))
      files
  in
  Format.eprintf "Opened files@." ;
  match R.create (Array.of_list ics) with
  | (stream, { bad_states = [] }) -> k stream
  | (stream, errs) ->
      Format.eprintf
        "Main.reconstruct: error opening files@.%a@."
        R.pp_error
        errs ;
      k stream
 [@@ocaml.inline]

(* Wrap the result of [reconstruct] in a Seq.t *)
let reconstruct_seq :
    type t s elt.
    (module Intf.Mux_stream
       with type local_stream = s
        and type t = t
        and type elt = elt) ->
    (in_channel -> s) ->
    string list ->
    (elt Seq.t -> 'b) ->
    'b =
 fun m from_chan files k ->
  let (module R) = m in
  reconstruct m from_chan files @@ fun s ->
  let seq =
    Seq.unfold
      (fun x ->
        match R.next x with
        | Result.Ok x -> x
        | Result.Error e ->
            Format.eprintf "error: %a" R.pp_error e ;
            None)
      s
  in
  k seq
 [@@ocaml.inline]

(* Subsamples a time series every [period]. *)
let subsample : type a. period:float -> (a -> float) -> a Seq.t -> a Seq.t =
 fun ~period now stream ->
  match Seq.uncons stream with
  | None -> Seq.empty
  | Some (head, tail) ->
      let t0 = now head in
      let result_tail =
        Seq.scan
          (fun (_, last_time) elt ->
            let t = now elt in
            if t -. last_time >= period then (Some elt, last_time +. period)
            else (None, last_time))
          (None, t0)
          tail
        |> Seq.filter_map (function ((Some _ as res), _) -> res | _ -> None)
      in
      Seq.cons head result_tail

(* Test *)
(* let () =
 *   let times = List.to_seq [0.0; 0.5; 1.0; 0.75; 2.1; 2.9; 3.0] in
 *   let subsampled_1 = List.of_seq (subsample ~period:1.0 Fun.id times) in
 *   List.iter (Printf.printf "%f ") subsampled_1 *)
(* 0.000000 1.000000 2.100000 3.000000 *)

(* Computes the induced subnetwork of all nodes whose topics intersects
   [topic]. *)
let restrict_graph_to_topic (graph : Digraph.t)
    (meta : Digraph.vertex -> Reconstruct.meta option) topic =
  let restricted = Digraph.create () in
  Digraph.iter_vertex
    (fun v ->
      match meta v with
      | None -> ()
      | Some (_, t) when Reconstruct.Input.topic_intersects t topic ->
          Digraph.add_vertex restricted v ;
          Digraph.iter_succ
            (fun v' ->
              match meta v' with
              | None -> ()
              | Some (_, t') when Reconstruct.Input.topic_intersects t' topic ->
                  Digraph.add_edge restricted v v'
              | Some _ -> ())
            graph
            v
      | Some _ -> ())
    graph ;
  restricted

let restrict_to_topic topic (seq : Reconstruct.elt Seq.t) =
  Seq.map
    (fun ({ Reconstruct.graph; metadata; _ } as elt) ->
      let graph = restrict_graph_to_topic graph metadata topic in
      { elt with graph })
    seq

(* Raw multiplexing (used for merging logs without processing them) *)
module Raw =
  Sheaf.Mux.Make
    (struct
      type t = Reconstruct.Input.t

      let compare n1 n2 =
        Float.compare (Reconstruct.Input.time n1) (Reconstruct.Input.time n2)
    end)
    (Reconstruct.Input_stream)

(* ---- *)

(* Compute the stream of connected components and write the result of [oc] *)
let cc seq oc =
  let fmtr = Format.formatter_of_out_channel oc in
  let t0 = ref (-1.0) in
  Seq.iter
    (fun { Reconstruct.t; graph; _ } ->
      let t =
        (* subtract first timestamp from all subsequent timestamps *)
        if !t0 < 0.0 then (
          t0 := t ;
          0.0)
        else t -. !t0
      in
      let ccs = Statistics.connected_components graph in
      let szs =
        Array.map List.length ccs |> Array.to_list
        |> List.sort (fun c1 c2 -> Int.compare c2 c1)
      in
      Format.printf "\rt = %f%!" t ;
      Format.fprintf
        fmtr
        "%f %a\n"
        t
        (Format.pp_print_list
           ~pp_sep:(fun fmtr () -> Format.fprintf fmtr " ")
           Format.pp_print_int)
        szs)
    seq

(* Compute the stream of connected components of topic subgraphs and write the result of [oc] *)
let cc_with_topic topic seq oc = cc (restrict_to_topic topic seq) oc

(* Compute the shortest path length between two nodes on a topic subgraph. *)
let path_length id1 id2 topic seq oc =
  let fmtr = Format.formatter_of_out_channel oc in
  let c = ref 0 in
  let t0 = ref (-1.0) in
  let v1 = Reconstruct.vertex id1 in
  let v2 = Reconstruct.vertex id2 in
  Seq.iter
    (fun { Reconstruct.t; graph; ids = _; metadata; diff = _; self = _ } ->
      Format.printf "\r %d%!" !c ;
      incr c ;
      let t =
        if !t0 < 0.0 then (
          t0 := t ;
          0.0)
        else t -. !t0
      in
      let graph = restrict_graph_to_topic graph metadata topic in
      match Statistics.path_length graph v1 v2 with
      | None -> Format.fprintf fmtr "%f 0\n" t
      | Some len -> Format.fprintf fmtr "%f %d\n" t len)
    seq

(* Compute the degree distribution. *)
let degree_dist topic seq oc =
  let fmtr = Format.formatter_of_out_channel oc in
  let t0 = ref (-1.0) in
  Seq.iter
    (fun { Reconstruct.t; graph; ids = _; metadata; diff = _; self = _ } ->
      let t =
        if !t0 < 0.0 then (
          t0 := t ;
          0.0)
        else t -. !t0
      in
      Format.printf "\rt = %f%!" t ;

      let graph = restrict_graph_to_topic graph metadata topic in
      let degrees =
        Digraph.fold_vertex
          (fun v acc -> Digraph.out_degree graph v :: acc)
          graph
          []
        |> List.sort (fun d1 d2 -> Int.compare d2 d1)
      in
      let count = Digraph.nb_vertex graph in
      let (min, max, sum) =
        List.fold_left
          (fun (min, max, sum) degree ->
            (Int.min min degree, Int.max max degree, sum + degree))
          (Int.max_int, Int.min_int, 0)
          degrees
      in
      let avg = float_of_int sum /. float_of_int count in
      Format.fprintf fmtr "%f %d %d %d %f " t count min max avg ;
      List.iter (fun d -> Format.fprintf fmtr "%d " d) degrees ;
      Format.fprintf fmtr "\n")
    seq

let diameter topic seq oc =
  let fmtr = Format.formatter_of_out_channel oc in
  let t0 = ref (-1.0) in
  Seq.iter
    (fun { Reconstruct.t; graph; ids = _; metadata; diff = _; self = _ } ->
      let t =
        if !t0 < 0.0 then (
          t0 := t ;
          0.0)
        else t -. !t0
      in
      Format.printf "\rt = %f%!" t ;
      let graph = restrict_graph_to_topic graph metadata topic in
      let diameter = Statistics.diameter graph in
      match diameter with
      | Stats.Graph.Dist.Inf -> Format.fprintf fmtr "%f inf\n" t
      | Stats.Graph.Dist.Fin diam -> Format.fprintf fmtr "%f %d\n" t diam)
    seq

let starting_times topic seq oc =
  let fmtr = Format.formatter_of_out_channel oc in
  let t0 = ref (-1.0) in
  let seen = Digraph.V.Table.create 1000 in
  (* A bit wasteful to reconstruct the graph for this command *)
  Seq.iter
    (fun { Reconstruct.t; graph = _; ids = _; metadata; diff; self } ->
      let t =
        if !t0 < 0.0 then (
          t0 := t ;
          0.0)
        else t -. !t0
      in
      Format.printf "\rt = %f%!" t ;
      let vertex = diff.vertex in
      match metadata vertex with
      | None -> ()
      | Some (_, vt) ->
          if Reconstruct.Input.topic_intersects topic vt then
            if not (Digraph.V.Table.mem seen vertex) then (
              Digraph.V.Table.add seen vertex () ;
              Format.fprintf fmtr "%s %f\n" self t)
            else ()
          else ())
    seq

let process_stream outfile infiles period_opt f =
  Out_channel.with_open_text outfile (fun oc ->
      reconstruct_seq
        (module Reconstruct)
        Reconstruct.Input_stream.from_channel
        infiles
      @@ fun seq ->
      match period_opt with
      | None -> f seq oc
      | Some period ->
          let seq = subsample ~period (fun { Reconstruct.t; _ } -> t) seq in
          f seq oc)

(* ---- *)

open Tezos_clic

let file_param =
  Clic.param
    ~name:"FILE"
    ~desc:"Log file"
    (Clic.parameter (fun _ s -> Lwt.return_ok s))

let replay_cmd =
  let open Clic in
  command
    ~desc:"GUI replaying the graph evolution"
    no_options
    (prefix "replay" @@ seq_of_param file_param)
  @@ fun () files ->
  reconstruct (module Reconstruct) Reconstruct.Input_stream.from_channel files
  @@ fun stream ->
  let sm_params =
    Springmodel.parameters
      ~stiffness:100.0
      ~relax_length:30.
      ~drag_factor:1.0
      ~coulomb_factor:1.0
      ~radius:50.
      ~dt:0.01
  in
  let state = Display.prepare_global 1024 768 sm_params stream in
  Display.loop state

let topic_param =
  Clic.param
    ~name:"TOPIC"
    ~desc:"Topic"
    (Clic.parameter (fun _ s ->
         match Reconstruct.Input.topic_of_string s with
         | None ->
             Tezos_error_monad.Error_monad.failwith
               "Could not parse topic %s@."
               s
         | Some topic -> Lwt.return_ok (s, topic)))

let target_file =
  Clic.arg
    ~doc:"File where to write the result"
    ~long:"save-to"
    ~placeholder:"FILE"
    (Clic.parameter (fun () s -> Lwt.return_ok s))

let period =
  Clic.arg
    ~doc:"Period at which to perform requested computation"
    ~long:"period"
    ~placeholder:"seconds"
    (Clic.parameter (fun () s ->
         match float_of_string s with
         | exception _ ->
             Tezos_error_monad.Error_monad.failwith
               "period: could not parse float"
         | f when f <= 0.0 ->
             Tezos_error_monad.Error_monad.failwith
               "period: float must be strictly positive"
         | f -> Lwt.return_ok f))

let cc_with_topic_cmd =
  let open Clic in
  command
    ~desc:"Compute connected components on topic subgraph"
    (args2 target_file period)
    (prefixes ["cc"; "with"; "topic"] @@ topic_param @@ seq_of_param file_param)
  @@ fun (target_file, period) (s, topic) files () ->
  let printable_topic = String.map (function '/' -> '_' | c -> c) s in
  let filename =
    match target_file with
    | None -> Printf.sprintf "cc-with-%s.dat" printable_topic
    | Some fname -> fname
  in
  process_stream filename files period (fun seq oc ->
      cc_with_topic topic seq oc) ;
  Lwt.return_ok ()

let cc_full_cmd =
  let open Clic in
  command
    ~desc:"Compute connected components on full graph"
    (args2 target_file period)
    (prefixes ["cc"; "full"] @@ seq_of_param file_param)
  @@ fun (target_file, period) files () ->
  let filename =
    match target_file with None -> "cc.dat" | Some fname -> fname
  in
  process_stream filename files period (fun seq oc -> cc seq oc) ;
  Lwt.return_ok ()

let merge_cmd =
  let open Clic in
  command
    ~desc:"Mergesort many log files"
    (args1 target_file)
    (prefixes ["merge"] @@ seq_of_param file_param)
  @@ fun target_file files () ->
  let c = ref 0 in
  reconstruct_seq
    (module Raw)
    Reconstruct.Input_stream.from_channel
    files
    (fun s ->
      let filename =
        match target_file with None -> "merged.txt" | Some fname -> fname
      in
      Out_channel.with_open_text filename (fun oc ->
          let buf = Buffer.create 4096 in
          let json_per_sec = ref 0.0 in
          let t0 = ref (Unix.gettimeofday ()) in
          Seq.iter
            (fun row ->
              Format.printf "\r %d, %f/s%!" !c !json_per_sec ;
              incr c ;
              if !c mod 1000 = 0 then (
                let now = Unix.gettimeofday () in
                let bef = !t0 in
                t0 := now ;
                json_per_sec := 1000. /. (now -. bef)) ;
              let json =
                Data_encoding.Json.construct Reconstruct.Input.encoding row
              in
              let json = Helpers.to_yojson json in
              let json =
                `Assoc
                  [ ( "fd-sink-item.v0",
                      `Assoc [("event", `Assoc [("peers_dumping.v0", json)])] )
                  ]
              in
              Yojson.Basic.to_channel ~buf oc json)
            s)) ;
  Lwt.return_ok ()

let node_param () =
  Clic.param
    ~name:"ID"
    ~desc:"Node id"
    (Clic.parameter (fun _ s -> Lwt.return_ok s))

let path_length_cmd =
  let open Clic in
  command
    ~desc:"Compute shortest path length on topic subgraph"
    (args2 target_file period)
    (prefixes ["path"; "length"; "from"]
    @@ node_param () @@ prefix "to" @@ node_param ()
    @@ prefixes ["on"; "topic"]
    @@ topic_param @@ seq_of_param file_param)
  @@ fun (target_file, period) id1 id2 (s, topic) files () ->
  let printable_topic = String.map (function '/' -> '_' | c -> c) s in
  let filename =
    match target_file with
    | None -> Printf.sprintf "cc-with-%s.dat" printable_topic
    | Some fname -> fname
  in
  process_stream filename files period (fun seq oc ->
      path_length id1 id2 topic seq oc) ;
  Lwt.return_ok ()

let degree_distribution_cmd =
  let open Clic in
  command
    ~desc:"Compute degree distribution on topic subgraph"
    (args2 target_file period)
    (prefixes ["degree"; "distribution"; "on"; "topic"]
    @@ topic_param @@ seq_of_param file_param)
  @@ fun (target_file, period) (s, topic) files () ->
  let printable_topic = String.map (function '/' -> '_' | c -> c) s in
  let filename =
    match target_file with
    | None -> Printf.sprintf "degree-dist-with-%s.dat" printable_topic
    | Some fname -> fname
  in
  process_stream filename files period (fun seq oc -> degree_dist topic seq oc) ;
  Lwt.return_ok ()

let diameter_cmd =
  let open Clic in
  command
    ~desc:"Compute diameter of topic subgraph"
    (args2 target_file period)
    (prefixes ["diameter"; "on"; "topic"]
    @@ topic_param @@ seq_of_param file_param)
  @@ fun (target_file, period) (s, topic) files () ->
  let printable_topic = String.map (function '/' -> '_' | c -> c) s in
  let filename =
    match target_file with
    | None -> Printf.sprintf "degree-dist-with-%s.dat" printable_topic
    | Some fname -> fname
  in
  process_stream filename files period (fun seq oc -> diameter topic seq oc) ;
  Lwt.return_ok ()

let startup_time_distribution_cmd =
  let open Clic in
  command
    ~desc:"Retrieve starting times of all nodes on topic subgraph"
    (args2 target_file period)
    (prefixes ["starting"; "times"; "on"; "topic"]
    @@ topic_param @@ seq_of_param file_param)
  @@ fun (target_file, period) (s, topic) files () ->
  let printable_topic = String.map (function '/' -> '_' | c -> c) s in
  let filename =
    match target_file with
    | None -> Printf.sprintf "degree-dist-with-%s.dat" printable_topic
    | Some fname -> fname
  in
  process_stream filename files period (fun seq oc ->
      starting_times topic seq oc) ;
  Lwt.return_ok ()

let all_commands =
  [ replay_cmd;
    cc_with_topic_cmd;
    cc_full_cmd;
    merge_cmd;
    path_length_cmd;
    degree_distribution_cmd;
    diameter_cmd;
    startup_time_distribution_cmd ]

let commands_with_man =
  Clic.add_manual
    ~executable_name:(Filename.basename Sys.executable_name)
    ~global_options:Clic.no_options
    (if Unix.isatty Unix.stdout then Clic.Ansi else Clic.Plain)
    Format.std_formatter
    all_commands

let (original_args, _autocomplete) =
  (* for shell aliases *)
  let rec move_autocomplete_token_upfront acc = function
    | "bash_autocomplete" :: prev_arg :: cur_arg :: script :: args ->
        let args = List.rev acc @ args in
        (args, Some (prev_arg, cur_arg, script))
    | x :: rest -> move_autocomplete_token_upfront (x :: acc) rest
    | [] -> (List.rev acc, None)
  in
  match Array.to_list Sys.argv with
  | _ :: args -> move_autocomplete_token_upfront [] args
  | [] -> ([], None)

let () =
  ignore
    Clic.(
      setup_formatter
        Format.std_formatter
        (if Unix.isatty Unix.stdout then Ansi else Plain)
        Short) ;
  let result =
    Lwt_main.run
      (let open Tezos_error_monad.Error_monad.Lwt_result_syntax in
      let* (list_flags, args) =
        Clic.parse_global_options Clic.no_options () original_args
      in
      match args with
      | [] -> return list_flags
      | _ ->
          let* () = Clic.dispatch commands_with_man () args in
          return list_flags)
  in
  match result with
  | Ok () -> ()
  | Error [Clic.Version] ->
      Format.printf "version alpha\n" ;
      exit 0
  | Error [Clic.Help command] ->
      Clic.usage
        Format.std_formatter
        ~executable_name:(Filename.basename Sys.executable_name)
        ~global_options:Clic.no_options
        (match command with None -> [] | Some c -> [c]) ;
      exit 0
  | Error errors ->
      Clic.pp_cli_errors
        Format.err_formatter
        ~executable_name:(Filename.basename Sys.executable_name)
        ~global_options:Clic.no_options
        ~default:(fun fmt err ->
          Tezos_error_monad.Error_monad.pp_print_trace fmt [err])
        errors ;
      exit 1
