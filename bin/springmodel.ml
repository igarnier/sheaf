(* Implementation of a spring model relaxation algorithm. We have objects in
   3d, with springs attached on some of them (no self-spring). Springs are
   parameterised by a stiffness constant. We want to minimise the total
   potential energy by adjusting the positions of the objects. We approximate
   this by computing the classical dynamics of each object, taking into account
   a global drag force. We stop whenever the total energy is below a given
   threshold.
   Our implementation is heavily imperative.
*)

open Sheaf
open Gg

(* ------------------------------------------------------------------------- *)

module Graph = Digraph
module Vertex = Digraph.V
module Vertex_table = Digraph.V.Table
module Vertex_set = Digraph.V.Set

type node_state =
  { mutable prev_position : P3.t;
    mutable position : P3.t;
    mutable velocity : V3.t;
    mutable force : V3.t
  }

type simulation_parameters =
  { radius : float;
    stiffness : float;
    relax_length : float;
    drag_factor : float;
    coulomb_factor : float;
    dt : float
  }

let parameters ~stiffness ~relax_length ~drag_factor ~coulomb_factor ~radius ~dt
    =
  { stiffness; relax_length; drag_factor; coulomb_factor; radius; dt }

(* A random point on the sphere centered on the origin with [radius] *)
let random_point_on_sphere radius =
  let theta0 = Random.float (2.0 *. Float.pi) in
  let theta1 = acos (1.0 -. Random.float 2.0) in
  let x = radius *. sin theta0 *. sin theta1 in
  let y = radius *. cos theta0 *. sin theta1 in
  let z = radius *. cos theta1 in
  P3.v x y z

let make_node_state radius =
  let pos = random_point_on_sphere radius in
  { prev_position = pos; position = pos; velocity = V3.zero; force = V3.zero }

let find_vstate radius simstate v =
  match Vertex_table.find_opt simstate v with
  | None ->
      let state = make_node_state radius in
      Vertex_table.add simstate v state ;
      state
  | Some state -> state

let project_to_sphere radius p =
  let norm = V3.norm p in
  let scaling = radius /. norm in
  V3.smul scaling p

let verlet pinned sm_params simstate graph dt =
  assert (dt > 0.0) ;
  let radius = sm_params.radius in
  Graph.iter_vertex
    (fun v ->
      let state = find_vstate radius simstate v in
      let prev_position = state.position in
      if Vertex_set.mem v pinned then (
        state.velocity <- V3.zero ;
        state.prev_position <- prev_position)
      else
        let open V3 in
        state.position <-
          project_to_sphere
            radius
            (V3.smul 2. state.position - state.prev_position
            + V3.smul (dt *. dt) state.force) ;
        state.velocity <-
          V3.smul (1. /. dt) (state.position - state.prev_position) ;
        state.prev_position <- prev_position)
    graph

let compute_forces sm_params simstate graph =
  (* Spring forces *)
  Graph.iter_vertex
    (fun v ->
      let state = find_vstate sm_params.radius simstate v in
      (* reset accumulator *)
      state.force <- V3.zero ;
      List.iteri
        (fun _ target ->
          let c = Vertex.compare v target in
          (* Compute force only once per edge. No loops. *)
          if c < 0 || not (Graph.mem_edge graph v target) then (
            let target_state = find_vstate sm_params.radius simstate target in
            let direction = V3.sub target_state.position state.position in
            (* from v to target *)
            let ndir = V3.norm direction in
            let delta_rel = ndir -. sm_params.relax_length in
            (* delta_rel > 0.0 -> stretched spring -> force towards target *)
            let force =
              delta_rel
              *. (* stiffness v target *)
              sm_params.stiffness
            in
            let forcevec = V3.smul (force /. ndir) direction in
            (* drag *)
            let drag = V3.smul ~-.(sm_params.drag_factor) state.velocity in
            let negdrag = V3.neg drag in
            state.force <- V3.add state.force forcevec ;
            state.force <- V3.add drag state.force ;
            target_state.force <- V3.add target_state.force (V3.neg forcevec) ;
            target_state.force <- V3.add negdrag target_state.force)
          else ())
        (Graph.succ graph v))
    graph ;
  (* Coulomb potential - not clear it's useful. *)
  if sm_params.coulomb_factor = 0.0 then ()
  else
    Graph.iter_vertex
      (fun vertex ->
        Graph.iter_vertex
          (fun vertex' ->
            if Vertex.equal vertex vertex' then ()
            else
              let open Gg.V3 in
              let state = find_vstate sm_params.radius simstate vertex in
              let state' = find_vstate sm_params.radius simstate vertex' in
              let diff = state'.position - state.position in
              let dist = norm diff in
              let idist = 1. /. dist in
              let dir = smul idist diff in
              let coulomb =
                smul (sm_params.coulomb_factor *. (idist *. idist)) dir
              in
              state.force <- state.force + neg coulomb ;
              state'.force <- state'.force + coulomb)
          graph)
      graph

let step pinned sm_params simstate graph =
  (* The vertices in [graph] and that in [simstate] might not be in sync anymore. *)
  (* TODO: GC dead vertices from [simstate] *)
  compute_forces sm_params simstate graph ;
  if sm_params.dt > 0.0 then verlet pinned sm_params simstate graph sm_params.dt

(* --- *)

type layout = Vertex.t -> P3.t

let process ~pinned ~sm_params ~sm_graph ~evolve =
  let open Cgraph.Infix in
  let simstate = Vertex_table.create 11 in
  let sm_graph =
    (* Resetting the velocity and prev_position of the modified vertex stabilizes the
       numerical integration (it makes discontinuities more manageable) *)
    let+ { Reconstruct.diff; graph; _ } = sm_graph and+ sm_params in
    let modified = find_vstate sm_params.radius simstate diff.vertex in
    modified.prev_position <- modified.position ;
    modified.velocity <- V3.zero ;
    graph
  in
  let+ sm_params and+ sm_graph and+ evolve and+ pinned in
  let layout v = (find_vstate sm_params.radius simstate v).position in
  if evolve then step pinned sm_params simstate sm_graph ;
  layout
