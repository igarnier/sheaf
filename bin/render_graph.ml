open Gg
open Sheaf
module Vertex_set = Digraph.V.Set

type vertex = Digraph.vertex

let p3_to_raylib (v : P3.t) = Raylib.Vector3.create (P3.x v) (P3.y v) (P3.z v)

let texture_vtable = Hashtbl.create 11

let rgb_from_color (c : Raylib.Color.t) =
  let open Raylib.Color in
  let r = r c in
  let g = g c in
  let b = b c in
  let a = a c in
  (r, g, b, a)

let texture_from_text text size color =
  let key = (text, size, rgb_from_color color) in
  match Hashtbl.find_opt texture_vtable key with
  | Some texture -> texture
  | None ->
      let text_img = Raylib.image_text text size color in
      let texture = Raylib.load_texture_from_image text_img in
      Hashtbl.add texture_vtable key texture ;
      texture

let draw_text camera pos size color text =
  let texture = texture_from_text text 10 color in
  Raylib.draw_billboard camera texture pos size color

let draw_node camera id pos =
  draw_text camera pos 1.0 Raylib.Color.black (string_of_int id)

let draw_edge _src _srcpos _tgt _tgtpos = ()

(* Todo:
   - a node can be both pinned and picked
   - edges from pinned to rest of the network should be highlighted differently perhaps
   - the way pinned node are displayed is weird
*)
type kind = Picked | Neighbour_of_picked | Pinned

let draw_scene :
    graph:Digraph.t ->
    layout:(Digraph.vertex -> Gg.P3.t) ->
    diff_queue:Sheaf.Reconstruction.diff Bounded_queue.t ->
    pinned:Vertex_set.t ->
    picked:vertex option ->
    camera:Raylib.Camera3D.t ->
    unit =
 fun ~graph ~layout ~diff_queue ~pinned ~picked ~camera ->
  let vtable = Hashtbl.create 11 in
  let etable = Hashtbl.create 11 in
  Bounded_queue.iteri
    (fun i { Reconstruction.vertex; added_neighbours; removed_neighbours } ->
      if vertex = -1 then ()
      else
        let opacity = 255 * i / diff_queue.max_length in
        if added_neighbours = [] && removed_neighbours = [] then
          let () =
            if not (Digraph.mem_vertex graph vertex) then
              Format.printf "vertex not found %d@." vertex
          in
          let () = assert (Digraph.mem_vertex graph vertex) in
          let color = Raylib.Color.create 0 0 255 opacity in
          List.iter
            (fun v' -> Hashtbl.add etable (vertex, v') color)
            (Digraph.succ graph vertex)
        else
          let color = Raylib.Color.create 255 0 0 opacity in
          List.iter
            (fun v' -> Hashtbl.add etable (vertex, v') color)
            added_neighbours)
    diff_queue ;
  Option.iter
    (fun vertex ->
      Hashtbl.add vtable vertex Picked ;
      Digraph.iter_succ
        (fun vertex' -> Hashtbl.add vtable vertex' Neighbour_of_picked)
        graph
        vertex)
    picked ;
  Vertex_set.iter (fun pinned -> Hashtbl.add vtable pinned Pinned) pinned ;
  Digraph.iter_edges
    (fun source_id target_id ->
      let source_pos = layout source_id in
      let target_pos = layout target_id in
      let vec = V3.sub target_pos source_pos in
      let unit_dir = V3.unit vec in
      let src = p3_to_raylib @@ V3.add source_pos unit_dir in
      let tgt = p3_to_raylib @@ V3.sub target_pos unit_dir in
      (match Hashtbl.find_opt etable (source_id, target_id) with
      | None -> Raylib.draw_line_3d src tgt Raylib.Color.black
      | Some clr -> Raylib.draw_line_3d src tgt clr) ;
      draw_edge source_id src target_id tgt)
    graph ;
  Digraph.iter_vertex
    (fun id ->
      let pos = p3_to_raylib (layout id) in
      draw_node camera id pos ;
      match Hashtbl.find_opt vtable id with
      | None -> ()
      | Some Picked -> Raylib.draw_cube_wires pos 5. 5. 5. Raylib.Color.red
      | Some Neighbour_of_picked ->
          Raylib.draw_cube_wires pos 5. 5. 5. Raylib.Color.blue
      | Some Pinned -> Raylib.draw_sphere_wires pos 5. 8 8 Raylib.Color.black)
    graph
