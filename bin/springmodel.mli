open Sheaf

type simulation_parameters =
  { radius : float;
    stiffness : float;
    relax_length : float;
    drag_factor : float;
    coulomb_factor : float;
    dt : float
  }

type layout = Digraph.vertex -> Gg.P3.t

val parameters :
  stiffness:float ->
  relax_length:float ->
  drag_factor:float ->
  coulomb_factor:float ->
  radius:float ->
  dt:float ->
  simulation_parameters

val process :
  pinned:Digraph.V.Set.t Cgraph.t ->
  sm_params:simulation_parameters Cgraph.t ->
  sm_graph:Reconstruct.elt Cgraph.t ->
  evolve:bool Cgraph.t ->
  layout Cgraph.t
